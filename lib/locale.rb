class Locale
  include ActiveModel::Validations

  attr_reader :lang

  module LANG
    RU = 'ru'.freeze
    EN = 'en'.freeze
  end

  validates :lang, inclusion: { in: [LANG::RU, LANG::EN] }

  def initialize(lang: nil)
    self.lang = lang || LANG::EN
  end

  private

  attr_writer :lang
end
