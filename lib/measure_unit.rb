# by default we keep metric values
class MeasureUnit
  include ActiveModel::Validations

  ROUND_FOR_FAHRENHEIT = 1

  MPS_TO_MPH_BASE = 2.236936
  ROUND_FOR_MPH = 2

  module UNIT
    METRIC   = 'metric'.freeze
    IMPERIAL = 'imperial'.freeze
  end

  attr_reader :measure_unit

  validates :measure_unit, inclusion: { in: [UNIT::METRIC, UNIT::IMPERIAL] }

  def initialize(measure_unit: nil)
    self.measure_unit = measure_unit || UNIT::METRIC
  end

  def self.to_fahrenheit(temperature)
    (temperature.to_f * 1.8 + 32).round(ROUND_FOR_FAHRENHEIT)
  end

  def self.to_miles_per_hour(speed)
    (speed * MPS_TO_MPH_BASE).round(ROUND_FOR_MPH)
  end

  private

  attr_writer :measure_unit
end
