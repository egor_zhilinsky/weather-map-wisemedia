# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170507093300) do

  create_table "cities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name_ru",             limit: 200,                   null: false
    t.string   "name_en",             limit: 200,                   null: false
    t.text     "description_ru",      limit: 65535
    t.text     "description_en",      limit: 65535
    t.string   "avatar"
    t.boolean  "active",                            default: false
    t.integer  "owm_city_identifier",                               null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.index ["owm_city_identifier"], name: "index_cities_on_owm_city_identifier", using: :btree
  end

end
