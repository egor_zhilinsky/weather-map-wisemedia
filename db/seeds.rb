[
  {
    name_en: 'Tallinn',
    name_ru: 'Таллин',
    description_ru: 'Desc RU',
    description_en: 'Decs EN',
    owm_city_identifier: 588_409
  },
  {
    name_en: 'Tartu',
    name_ru: 'Тарту',
    description_ru: 'Desc RU',
    description_en: 'Decs EN',
    owm_city_identifier: 588_335
  },
  {
    name_en: 'Brno',
    name_ru: 'Брно',
    description_ru: 'Desc RU',
    description_en: 'Decs EN',
    owm_city_identifier: 3_078_610
  }
].each { |city_params| City.create!(city_params) }
