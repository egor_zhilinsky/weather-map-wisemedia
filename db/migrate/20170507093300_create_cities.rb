class CreateCities < ActiveRecord::Migration[5.0]
  def change
    create_table :cities do |t|
      t.string  :name_ru, limit: 200, null: false
      t.string  :name_en, limit: 200, null: false
      t.text    :description_ru, limit: 5000
      t.text    :description_en, limit: 5000
      t.string  :avatar, limit: 255
      t.boolean :active, default: false
      t.integer :owm_city_identifier, null: false, index: true
      t.timestamps
    end
  end
end
