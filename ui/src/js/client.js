import React    from "react";
import ReactDOM from "react-dom";
import {
  Provider
} from "react-redux";

import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";

import Layout from "./components/Layout";

import CitiesList from "./components/City/CitiesList";
import CityShow from "./components/City/CityShow";
import NotFound from "./components/NotFound";

import store from "./store";

const app = document.getElementById('app')

ReactDOM.render(<Provider store={store}>
  <Layout>
    <Router>
      <Switch>
        <Route exact path="/" component={CitiesList} />
        <Route path="/cities/:cityId" component={CityShow} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Router>
  </Layout>
</Provider>, app);
