import axios from "axios";
import { SERVER_BASE_URL } from './../constants';
import { cityListSchema, citySchema } from '../schema';

import { normalize } from 'normalizr';

function getCitiesUrl(defaults, cityId) {
  let urlParams = "?lang=" + defaults.lang + "&measure_unit=" + defaults.measure_unit
  let baseUrl = SERVER_BASE_URL + "/api/forecasts";
  if(cityId) {
    baseUrl += "/" + cityId;
  }

  return baseUrl + urlParams;
}

export function fetchCitiesForecastsAction(defaults) {
  return function(dispatch) {
    axios.get(getCitiesUrl(defaults, null))
      .then((response) => {
        const normalized = normalize(response.data.data.cities, cityListSchema);
        dispatch({type: "FETCH_FORECASTS_FULFILLED", payload: normalized})
      })
      .catch((err) => {
        dispatch({type: "FETCH_FORECASTS_REJECTED", payload: err})
      })
  }
}


export function cityFullForecastAction(defaults, cityId) {
  return function(dispatch) {
    axios.get(getCitiesUrl(defaults, cityId))
      .then((response) => {
        const normalized = normalize(response.data.data.city, citySchema);
        dispatch({type: "UPDATE_CITY_SUCCESSED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "UPDATE_CITY_REJECTED", payload: err})
      })
  }
}
