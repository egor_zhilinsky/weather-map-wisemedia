export function updateLangAction(newLanguage) {
  return function(dispatch) {
    dispatch({type: "UPDATE_LANGUAGE", payload: newLanguage});
     window.location.reload();
  }
}

export function updateMeasureUnitAction(newMeasureUnit) {
  return function(dispatch) {
    dispatch({type: "UPDATE_MEASURE_UNIT", payload: newMeasureUnit});
    window.location.reload();
  }
}
