export default function reducer(state={
    data:      {},
    fetching:  false,
    fetched:   false,
    error:     null,
    order:     []
  }, action) {

  switch (action.type) {
    case "FETCH_FORECASTS": {
      return {
        ...state,
        fetching: true
      }
    }
    case "FETCH_FORECASTS_REJECTED": {
      return {
        ...state,
        fetching: false,
        error: action.payload
      }
    }
    case "FETCH_FORECASTS_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        data: action.payload.entities.cities,
        order: action.payload.result
      }
    }
    case "UPDATE_CITY_SUCCESSED": {
      let newData = state.data;
      let cityData = action.payload.data.city;
      newData[cityData.id.toString()] = cityData
      return {
        ...state,
        fetching: false,
        fetched: true,
        data: newData
      }
    }
  }

  return state;
};
