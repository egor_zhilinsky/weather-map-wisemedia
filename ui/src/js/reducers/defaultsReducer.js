export default function reducer(state = {
    lang: localStorage.getItem("lang") || "en",
    measure_unit: localStorage.getItem("measure_unit") || "metric"
  }, action) {

  switch (action.type) {
    case "UPDATE_LANGUAGE": {
      localStorage.setItem("lang", action.payload.lang)
      return state;
    }
    case "UPDATE_MEASURE_UNIT": {
      localStorage.setItem("measure_unit", action.payload.measure)
      return state;
    }
  };

  return state;
};
