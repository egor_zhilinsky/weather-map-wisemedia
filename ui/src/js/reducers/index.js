import {
  combineReducers
} from "redux"

import cities from "./citiesReducer"
import defaults from "./defaultsReducer"

export default combineReducers({cities, defaults});
