import React from "react"
export default class WeatherIcon extends React.Component {

  render() {
    const forecastImageUrl = 'http://openweathermap.org/img/w/' + this.props.weatherIcon + '.png';
    return <img src={forecastImageUrl} />
  }
}
