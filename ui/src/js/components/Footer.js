import React from "react"

export default class Footer extends React.Component {

  render() {
    return <footer class="footer">
      <div class="container">
        <span class="text-muted">For Wisemedia by <a href="http://zhylinski.me">Yahor Zhylinski</a></span>
      </div>
    </footer>
  }
}
