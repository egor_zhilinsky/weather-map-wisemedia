import React from "react";
import { connect } from "react-redux";

import CityPreview from "./CityPreview"

import { fetchCitiesForecastsAction } from "../../actions/citiesForecastsActions";

@connect((store) => {
  return {
    cities: store.cities,
    defaults: store.defaults
  };
})
export default class CitiesList extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.dispatch(fetchCitiesForecastsAction(this.props.defaults))
  }

  render() {
    const { data, order } = this.props.cities;
    const mappedCities = order.map(cityId => <CityPreview key={cityId} city={data[cityId.toString()]} />);
    return <div>{mappedCities}</div>
  }
}
