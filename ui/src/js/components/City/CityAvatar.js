import React from "react"
import { SERVER_BASE_URL } from './../../constants';

export default class CityAvatar extends React.Component {

  render() {
    const className = "city-avatar-" + this.props.imageType;
    return <img className={className} src={SERVER_BASE_URL + this.props.imageUrl} />
  }
}
