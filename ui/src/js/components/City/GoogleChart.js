import React           from 'react';
import { Chart }       from 'react-google-charts';
import DropdownButton  from 'react-bootstrap/lib/DropdownButton';
import MenuItem        from 'react-bootstrap/lib/MenuItem';
import MeasureUnit     from './../MeasureUnit';
import LocalizedString from './../LocalizedString';

export default class GoogleChart extends React.Component {
  constructor(props) {
    super(props);

    const forecastsTypes = [ 'temperature', 'wind_speed', 'wind_deg', 'humidity', 'pressure'];

    this.state = {
      forecastsTypes: forecastsTypes,
      selectedType: forecastsTypes[0]
    }
  }

  changeGraphic(eventKey) {
    this.setState({ selectedType : eventKey });
  }

  render() {
    const { forecasts } = this.props;

    const dropdownTypes = this.state.forecastsTypes.map(type =>
      <MenuItem onSelect={this.changeGraphic.bind(this)} key={type} eventKey={type}><LocalizedString stringKey={type} />(<MeasureUnit measureKey={type} />)</MenuItem>
    );

    var forecastsData = forecasts.map(forecast => ([new Date(forecast.datetime), forecast[this.state.selectedType]]))

    return(
      <div>
        <div className='text-right'>
          <DropdownButton pullRight title={
            <LocalizedString stringKey={this.state.selectedType}>
              (<MeasureUnit measureKey={this.state.selectedType} />)
            </LocalizedString>}
            id='bg-nested-dropdown'>
              {dropdownTypes}
          </DropdownButton>
        </div>

        <div className={'chart-container'}>
          <Chart
            chartType='LineChart'
            data={[['name', '']].concat(forecastsData)}
            options={{}}
            graph_id='forecasts-chart'
            width='100%'
            height='400px'
            legend_toggle
          />
        </div>
      </div>
    )
  }
}
