import React          from "react";
import { connect }    from "react-redux";
import GoogleChart    from './GoogleChart'
import WeatherIcon    from "./../WeatherIcon"
import capitalize     from 'capitalize';
import MeasureUnit    from './../MeasureUnit';
import CityAvatar     from './CityAvatar';

import { cityFullForecastAction } from "../../actions/citiesForecastsActions";

@connect((store, props) => {
  let fetchedData = store.cities.data[props.match.params.cityId] || {}
  return {
    city: fetchedData,
    fetched: store.cities.fetched,
    defaults: store.defaults
  };
})

export default class CityShow extends React.Component {
  componentWillMount() {
    this.props.dispatch(cityFullForecastAction(this.props.defaults, this.props.match.params.cityId));
  }

  render() {
    const { city, fetched } = this.props;

    if(!fetched) {
      return <div>LOADING</div>;
    }

    const { temperature, weather_icon: weatherIcon, weather_description: weatherDescription } = city.forecasts[0];

    return <div>
      <div className="col-xs-12">
        <div className="city-avatar">
          <CityAvatar imageUrl={city.avatar.url} imageType="default" />
        </div>

        <div className="col-xs-12 col-md-6">
          <h2 className="city-name">{city.name}</h2>
          <p>
            {temperature} <MeasureUnit measureKey="temperature" />
            <WeatherIcon weatherIcon={weatherIcon} />
          </p>
          <p>
            {capitalize(weatherDescription)}
          </p>
        </div>

        <div className="col-xs-12 city-description">
          {city.description}
        </div>
        <div className="col-xs-12">
          <GoogleChart forecasts={city.forecasts} />
        </div>
      </div>

    </div>
  }
}
