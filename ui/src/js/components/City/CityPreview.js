import React           from "react"
import { Link }        from "react-router-dom";
import WeatherIcon     from "./../WeatherIcon";
import LocalizedString from "./../LocalizedString";
import MeasureUnit     from "./../MeasureUnit";
import CityAvatar      from "./CityAvatar";
import capitalize      from "capitalize";

export default class CityPreview extends React.Component {
  render() {
    const { id, name, avatar, forecasts } = this.props.city;
    const cityForecast = forecasts[0] || {};

    return <div className="card col-xs-12 col-sm-6 col-md-4">
      <div className="city-preview-card col-xs-12">
        <div className="card-block">
          <h3 class="card-title">
            <div className="city-privew-card-avatar-container">
              <CityAvatar imageUrl={avatar.url} imageType="thumb" />
            </div>

            <strong className="city-name">{name}</strong>
            {cityForecast.temperature}
            <MeasureUnit measureKey="temperature" />
            <WeatherIcon weatherIcon={cityForecast.weather_icon} />
          </h3>

          <p class="card-text">
            {capitalize(cityForecast.weather_description)}
          </p>

          <Link className="btn btn-primary pull-right" to={`/cities/${id}`}>
            <LocalizedString stringKey="see_long_term_forecasts" />
          </Link>
        </div>
      </div>
    </div>
  }
}
