import React from "react"
import Footer from "./Footer";
import Navigation from "./Navigation";

export default class Layout extends React.Component {
  render() {
    return <div>
      <Navigation />
      <div className="container">
        <div className="row">
          {this.props.children}
        </div>
      </div>
      <Footer />
    </div>
  }
}
