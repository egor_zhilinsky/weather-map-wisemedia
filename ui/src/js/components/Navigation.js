import React from "react";
import { connect } from "react-redux";
import { Nav, Navbar, NavDropdown, MenuItem } from 'react-bootstrap';
import { updateLangAction, updateMeasureUnitAction } from "../actions/defaultsActions";
import LocalizedString from "./LocalizedString";
import { SERVER_BASE_URL } from './../constants';

@connect(function(state) {
  return state.defaults;
})

export default class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  changeLocale(locale) {
    this.props.dispatch(updateLangAction(locale))
  }

  changeMeasureUnit(measureUnit) {
    this.props.dispatch(updateMeasureUnitAction(measureUnit))
  }

  openTools(params) {
    var win = window.open(SERVER_BASE_URL + params.url, '_blank');
    win.focus();
  }

  render() {
    return <Navbar inverse collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="/">WISEMEDIA</a>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav>
          <li role="presentation"><a href="/"><LocalizedString stringKey="forecasts_btn_navbar" /></a></li>
        </Nav>

        <Nav pullRight>
          <NavDropdown eventKey={3} title={<LocalizedString stringKey="tools_navbar" />} id="language-dropdown">
            <MenuItem onSelect={this.openTools.bind(this)} eventKey={({url: '/coverage/index.html'})}>Coverage</MenuItem>
            <MenuItem onSelect={this.openTools.bind(this)} eventKey={({url: '/admin'})}>Admin</MenuItem>
            <MenuItem onSelect={this.openTools.bind(this)} eventKey={({url: '/resque/overview'})}>Resque</MenuItem>
            <MenuItem onSelect={this.openTools.bind(this)} eventKey={({url: '/apipie'})}>Apipie</MenuItem>
          </NavDropdown>

          <NavDropdown eventKey={3} title={<LocalizedString stringKey="language_navbar" />} id="language-dropdown">
            <MenuItem onSelect={this.changeLocale.bind(this)} eventKey={({lang: 'en'})}>English</MenuItem>
            <MenuItem onSelect={this.changeLocale.bind(this)} eventKey={({lang: 'ru'})}>Русский</MenuItem>
          </NavDropdown>
          <NavDropdown eventKey={4} title={<LocalizedString stringKey="measure_unit_navbar" />} id="measure-dropdown">
            <MenuItem onSelect={this.changeMeasureUnit.bind(this)} eventKey={({measure: 'metric'})}><LocalizedString stringKey="measure_unit_metric_navbar" /></MenuItem>
            <MenuItem onSelect={this.changeMeasureUnit.bind(this)} eventKey={({measure: 'imperial'})}><LocalizedString stringKey="measure_unit_imperial_navbar" /></MenuItem>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  }
}
