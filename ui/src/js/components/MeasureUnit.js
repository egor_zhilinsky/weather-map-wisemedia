import React       from "react";
import LocalizedStrings from 'react-localization';
import { connect } from "react-redux";

@connect(function(state) {
  return state.defaults;
})

export default class MeasureUnit extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const localizedStrings = new LocalizedStrings({
     en: {
       temperature: {
         imperial: "°F",
         metric: "°C"
       },
       wind_speed: {
         imperial: "MPH",
         metric: "Met/Sec"
       },
       wind_deg: {
         imperial: "°",
         metric: "°"
       },
       humidity: {
         imperial: "%",
         metric: "%"
       },
       pressure: {
         imperial: "hPa",
         metric: "hPa"
       }
     },
     ru: {
       temperature: {
         imperial: "°F",
         metric: "°C"
       },
       wind_speed: {
         imperial: "Миль в час",
         metric: "Метр/сек"
       },
       wind_deg: {
         imperial: "°",
         metric: "°"
       },
       humidity: {
         imperial: "%",
         metric: "%"
       },
       pressure: {
         imperial: "гПа",
         metric: "гПа"
       }
     }
    });

    const { lang: language, measureKey, measure_unit: measureUnit } = this.props;

    localizedStrings.setLanguage(language);

    return <span>{localizedStrings[measureKey][measureUnit]}</span>;
  }
}
