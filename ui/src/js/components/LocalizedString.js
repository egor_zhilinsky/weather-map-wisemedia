import React       from "react";
import LocalizedStrings from 'react-localization';
import { connect } from "react-redux";

@connect(function(state) {
  return state.defaults;
})

export default class LocalizedString extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let localizedStrings = new LocalizedStrings({
     en: {
       see_long_term_forecasts: "See 3-days forecast",
       forecasts_btn_navbar: "Forecasts",
       language_navbar: "Language",
       measure_unit_navbar: "Measure unit",
       tools_navbar: "Tools",
       measure_unit_metric_navbar: "Metric",
       measure_unit_imperial_navbar: "Imperial",
       temperature: 'Temperature',
       wind_speed: 'Wind Speed',
       wind_deg: 'Wind Deg',
       humidity: 'Humidity',
       pressure: 'Pressure'
     },
     ru: {
       see_long_term_forecasts: "Прогноз на 3 дня",
       forecasts_btn_navbar: "Прогнозы",
       language_navbar: "Язык",
       measure_unit_navbar: "Ед. изм.",
       tools_navbar: "Утилиты",
       measure_unit_metric_navbar: "Метрическая",
       measure_unit_imperial_navbar: "Имперская",
       temperature: 'Температура',
       wind_speed: 'Скорость Ветра',
       wind_deg: 'Угол ветра',
       humidity: 'Влажность',
       pressure: 'Давление'
     }
    });

    const { lang: language, stringKey } = this.props;
    localizedStrings.setLanguage(language);

    return <span>
      {localizedStrings[stringKey]}
      {this.props.children}
    </span>;
  }
}
