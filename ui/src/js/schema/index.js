import { normalize, schema } from 'normalizr';
export const citySchema = new schema.Entity('cities');
export const cityListSchema = new schema.Array(citySchema);
