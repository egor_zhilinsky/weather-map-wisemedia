# Test task for Wisemedia

[![Watch the video](http://img.youtube.com/vi/kYSGjZef_R4/0.jpg)](https://youtu.be/kYSGjZef_R4)

## Backend

Ruby On Rails 5, ruby 2.4.0, Rspec, Rubocop, Redis, Mysql, Singleton, Decorators, Proxy, Builder, MiniMagick, carrierwave, activeadmin, openstreetmap

## Frontend

react, redux, bootstrap, normilizer, axios