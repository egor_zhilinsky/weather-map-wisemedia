source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'active_scheduler'
gem 'activeadmin'
gem 'apipie-rails'
gem 'carrierwave', '~> 1.0'
gem 'foreman'
gem 'mini_magick'
gem 'mysql2'
gem 'open-weather'
gem 'puma', '~> 3.0'
gem 'rack-cors'
gem 'rails', '~> 5.0.1'
gem 'redis'
gem 'redis-objects'
gem 'resque'
gem 'resque-scheduler'

group :development do
  gem 'annotate', require: false
  gem 'listen', '~> 3.0.5'
  gem 'pre-commit', require: false
  gem 'rubocop', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'factory_girl_rails'
  gem 'json_matchers'
  gem 'rspec-rails', '~> 3.5'
  gem 'shoulda-matchers', git: 'https://github.com/thoughtbot/shoulda-matchers.git', branch: 'rails-5'
  gem 'simplecov', require: false
  gem 'webmock'
end

group :test, :development do
  gem 'pry'
end
