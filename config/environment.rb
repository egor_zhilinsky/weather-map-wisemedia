# Load the Rails application.
require_relative 'application'

unless ENV['SECRETS_LOADED']
  secret_env_data = File.join(Rails.root, 'config', 'secrets.rb')
  raise 'Missing secrets file' unless File.exist?(secret_env_data)

  load(secret_env_data)
end

# Initialize the Rails application.
Rails.application.initialize!
