require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module WeatherMapWisemedia
  class Application < Rails::Application
    config.active_job.queue_adapter = :resque
    config.autoload_paths << Rails.root.join('app/api_docs')
    config.autoload_paths << Rails.root.join('app/decorators')
    config.autoload_paths << Rails.root.join('lib')
    config.i18n.default_locale = :en
  end
end
