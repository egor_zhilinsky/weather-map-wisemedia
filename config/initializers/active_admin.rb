ActiveAdmin.setup do |config|
  config.site_title = 'Weather Map Wisemedia'
  config.logout_link_path = :destroy_admin_user_session_path
  config.comments = false
  config.batch_actions = true
  config.localize_format = :long
  config.root_to = 'cities#index'
end
