require 'redis'
require 'redis/objects'
require 'singleton'

class RedisServer
  include Singleton

  def initialize
    config = File.read(Rails.root.join('config/redis.yml'))
    redis_yml_config = YAML.safe_load(ERB.new(config).result(binding)).symbolize_keys

    defaults = redis_yml_config[:default].symbolize_keys
    redis_config = defaults.merge(redis_yml_config[Rails.env.to_sym].symbolize_keys) if redis_yml_config[Rails.env.to_sym]

    self.redis_instance = Redis.new(redis_config)
    Redis::Objects.redis = redis_instance
    redis_instance.flushdb if Rails.env == 'test'
  end

  def get(key)
    redis_instance.get(key)
  end

  def set(key, value)
    redis_instance.set(key, value)
  end

  private

  attr_accessor :redis_instance
end
