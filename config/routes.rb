require 'resque/server'
Rails.application.routes.draw do
  ActiveAdmin.routes(self)

  namespace :api do
    resources :forecasts, only: %I[index show]
  end

  mount Resque::Server.new, at: '/resque'
  apipie
end
