module Api
  class ForecastsController < ApiController
    include ::Api::ForecastsDoc

    before_action :load_city, only: [:show]
    before_action :load_measure_unit_param!

    def show
      respond_with(Forecasts::CityForecastsPresenter.new(data: city,
        lang: locale.to_s, measure_unit: measure_unit.measure_unit))
    end

    def index
      respond_with(Forecasts::CitiesForecastsPresenter.new(data: City.active,
        lang: locale.to_s, measure_unit: measure_unit.measure_unit))
    end

    private

    attr_accessor :city, :measure_unit

    def load_city
      self.city = City.active.find(params[:id])
      return head(404) unless city
    end

    def load_measure_unit_param!
      self.measure_unit = MeasureUnit.new(measure_unit: params[:measure_unit])
      respond_with(ErrorPresenter.new(data: measure_unit.errors), :bad_request) if measure_unit.invalid?
    end
  end
end
