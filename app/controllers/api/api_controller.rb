module Api
  class ApiController < ActionController::API
    before_action :set_locale!

    rescue_from ActiveRecord::RecordNotFound do
      head(:not_found)
    end

    private

    def set_locale!
      locale = Locale.new(lang: params[:lang])
      if locale.valid?
        self.locale = locale.lang
      else
        respond_with(ErrorPresenter.new(data: locale.errors), :bad_request)
      end
    end

    def respond_with(data, status = :ok)
      render(json: { data: data.as_json }, status: status)
    end

    attr_accessor :locale
  end
end
