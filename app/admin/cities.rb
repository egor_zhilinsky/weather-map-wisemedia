ActiveAdmin.register City do
  permit_params :name_ru, :name_en, :description_ru, :description_en, :avatar, :owm_city_identifier, :active

  index do
    column :name_en
    column :name_ru
    column :avatar do |raw|
      image_tag(raw.avatar.thumb.url)
    end
    column :owm_city_identifier
    column :active
    actions
  end
end
