class CityAvatarUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url(*_args)
    "/defaults/city_#{version_name}.png"
  end

  version :thumb do
    process resize_to_fit: [50, 50]
  end

  version :middle do
    process resize_to_fit: [200, 200]
  end
end
