class UpdateForecastsJob < ActiveJob::Base
  queue_as :update_forecasts

  def perform
    City.all.each do |city|
      forecasts = Forecasts::OwmAdapter.new.forecasts_for(city, Forecasts::CachedCityForecast)
      Forecasts::CacheProxy.new.update_forecasts_for(city.owm_city_identifier, forecasts)
    end
  end
end
