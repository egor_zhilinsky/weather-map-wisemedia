class CityLanguageDecorator
  attr_reader :city, :language

  def initialize(city:, language:)
    self.city = city
    self.language = language
  end

  def id
    city.id
  end

  def avatar
    city.avatar
  end

  def name
    language == Locale::LANG::RU ? city.name_ru : city.name_en
  end

  def description
    language == Locale::LANG::RU ? city.description_ru : city.description_en
  end

  private

  attr_writer :city, :language
end
