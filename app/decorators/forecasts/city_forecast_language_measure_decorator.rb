module Forecasts
  class CityForecastLanguageMeasureDecorator
    attr_reader :forecast, :language, :measure_unit

    def initialize(forecast:, language:, measure_unit:)
      self.forecast = forecast
      self.language = language
      self.measure_unit = measure_unit
    end

    def datetime
      forecast.datetime
    end

    def humidity
      forecast.humidity
    end

    def pressure
      forecast.pressure
    end

    def wind_deg
      forecast.wind_deg
    end

    def wearher_description
      language == Locale::LANG::EN ? forecast.description_en : forecast.description_ru
    end

    def weather_icon
      forecast.weather_icon
    end

    def temperature
      measure_unit == MeasureUnit::UNIT::METRIC ? forecast.temperature : ::MeasureUnit.to_fahrenheit(forecast.temperature)
    end

    def wind_speed
      measure_unit == MeasureUnit::UNIT::METRIC ? forecast.wind_speed : ::MeasureUnit.to_miles_per_hour(forecast.wind_speed)
    end

    private

    attr_writer :forecast, :language, :measure_unit
  end
end
