module Api
  module ForecastsDoc
    extend Apipie::DSL::Concern

    api :GET, '/api/forecasts/', 'All cities current weather description'
    formats ['json']
    example <<HEREDOC
  Example Request
  Example Response
  {
    "data": {
      "cities": [
        {
          "id": 1,
          "name": "Таллин",
          "avatar": {
            "url": "/uploads/city/avatar/1/thumb_tallin-estonia-chto-posmotret.jpg"
          },
          "forecasts": [
            {
              "temperature": 14.91,
              "weather_icon": "02d",
              "weather_description": "облачно"
            }
          ]
        }
      ]
    }
  }
HEREDOC

    def index; end

    api :GET, '/api/forecasts/:city_id', 'City weather forecasts'
    param :city_id, Integer, desc: 'City ID', required: true
    formats ['json']
    example <<HEREDOC
  Example Request
  Example Response
  {
    "data": {
      "city": {
        "id": 4,
        "name": "Минск",
        "avatar": {
          "url":"/uploads/city/avatar/4/middle_Minsk._A_view_of_Svislach_river.jpg"
        },
        "description":"Минск...",
        "forecasts": [
          {
            "datetime": 1496048400000,
            "humidity":57,
            "pressure":995.19,
            "wind_deg":255.503,
            "wind_speed":9.04,
            "temperature":22.86,
            "weather_icon":"10d",
            "weather_description":
            "легкий дождь"
          }
        ]
      }
    }
  }
HEREDOC
    def show; end
  end
end
