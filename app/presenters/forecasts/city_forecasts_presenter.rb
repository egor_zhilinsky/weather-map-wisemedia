module Forecasts
  class CityForecastsPresenter < ForecastsPresenter
    def initialize(data:, measure_unit:, lang:)
      super(measure_unit: measure_unit, lang: lang)
      self.city = data
    end

    def as_json(_options = nil)
      { city: city_json.merge(forecasts: forecasts_json) }
    end

    def city_json
      city_decorator = ::CityLanguageDecorator.new(city: city, language: lang)
      {
        id:          city_decorator.id,
        name:        city_decorator.name,
        avatar:      city_decorator.avatar.middle,
        description: city_decorator.description
      }
    end

    def forecasts_json
      city.forecasts.map do |forecast|
        forecast_decorator = CityForecastLanguageMeasureDecorator.new(forecast: forecast,
        language: lang, measure_unit: measure_unit)
        {
          datetime:     forecast_decorator.datetime,
          humidity:     forecast_decorator.humidity,
          pressure:     forecast_decorator.pressure,
          wind_deg:     forecast_decorator.wind_deg,
          wind_speed:   forecast_decorator.wind_speed,
          temperature:  forecast_decorator.temperature,
          weather_icon: forecast_decorator.weather_icon,
          weather_description: forecast_decorator.wearher_description
        }
      end
    end

    private

    attr_accessor :city
  end
end
