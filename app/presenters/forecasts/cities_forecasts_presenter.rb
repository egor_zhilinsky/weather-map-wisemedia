module Forecasts
  class CitiesForecastsPresenter < ForecastsPresenter
    def initialize(data:, lang:, measure_unit:)
      super(lang: lang, measure_unit: measure_unit)
      self.cities = data
    end

    def as_json(_options = nil)
      cities_json = cities.map do |city|
        city_decorator = ::CityLanguageDecorator.new(city: city, language: lang)
        {
          id:     city_decorator.id,
          name:   city_decorator.name,
          avatar: city_decorator.avatar.thumb,
          forecasts: forecasts_json(city)
        }
      end

      { cities: cities_json }
    end

    def forecasts_json(city)
      city.forecasts[0, 3].map do |forecast|
        forecast_decorator = CityForecastLanguageMeasureDecorator.new(forecast: forecast,
          language: lang, measure_unit: measure_unit)
        {
          temperature:  forecast_decorator.temperature,
          weather_icon: forecast_decorator.weather_icon,
          weather_description: forecast_decorator.wearher_description
        }
      end
    end

    private

    attr_accessor :cities
  end
end
