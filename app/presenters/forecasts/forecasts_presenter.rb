module Forecasts
  class ForecastsPresenter
    def initialize(lang:, measure_unit:)
      self.lang = lang
      self.measure_unit = measure_unit
    end

    private

    attr_accessor :lang, :measure_unit
  end
end
