class ErrorPresenter
  def initialize(data:)
    self.data = data
  end

  def as_json(_options = nil)
    {
      errors: data
    }
  end

  private

  attr_accessor :data
end
