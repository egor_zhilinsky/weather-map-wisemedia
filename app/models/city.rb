# == Schema Information
#
# Table name: cities
#
#  id                  :integer          not null, primary key
#  name_ru             :string(200)      not null
#  name_en             :string(200)      not null
#  description_ru      :text(65535)
#  description_en      :text(65535)
#  avatar              :string(255)
#  active              :boolean          default("0")
#  owm_city_identifier :integer          not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class City < ApplicationRecord
  CITY_ID_IS_REQUIRED = true
  CITY_ID_IS_UNIQ     = true

  NAME_IS_REQUIRED    = true
  NAME_MAX_LENGTH     = 200

  DESCRIPTION_MAX_LENGTH = 5_000

  mount_uploader :avatar, CityAvatarUploader

  validates :owm_city_identifier,
            presence: CITY_ID_IS_REQUIRED,
            uniqueness: CITY_ID_IS_UNIQ

  validates :name_ru, :name_en,
            presence: NAME_IS_REQUIRED,
            length: { maximum: NAME_MAX_LENGTH }

  validates :description_ru, :description_en,
            length: { maximum: DESCRIPTION_MAX_LENGTH }

  scope(:active, -> { where(active: true) })

  def forecasts
    Forecasts::CacheProxy.new.all_for(owm_city_identifier)
  end
end
