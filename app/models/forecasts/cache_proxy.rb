module Forecasts
  class CacheProxy
    REDIS_FORECAST_CACHE_KEY = 'forecast_for_:city_id'.freeze

    def initialize; end

    def all_for(city_id)
      cached_forecast_instance(city_id)
    end

    def update_forecasts_for(city_id, forecast_data)
      data_for_redis_cache = forecast_data.map(&:to_h)
      RedisServer.instance.set(cache_key_for_city(city_id), data_for_redis_cache.to_json)
    end

    private

    def cached_forecast_instance(city_id)
      cached_forecasts = JSON.parse(RedisServer.instance.get(cache_key_for_city(city_id)) || '{}')
      return [] unless cached_forecasts

      cached_forecasts.map do |forecast_params|
        CachedCityForecastBuilder.new(CachedCityForecast).with_params(forecast_params.symbolize_keys).build
      end
    end

    def cache_key_for_city(city_id)
      REDIS_FORECAST_CACHE_KEY.gsub(':city_id', city_id.to_s)
    end
  end
end
