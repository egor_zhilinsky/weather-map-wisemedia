require 'open_weather'

module Forecasts
  class OwmAdapter
    DEFAULT_OWM_PARAMS = {
      APPID: ENV['OPENWEATHERMAP_APPID'],
      units: 'metric',
      lang: ['ru']
    }.freeze

    def initialize; end

    def forecasts_for(city, forecast_class)
      owm_request = ::OpenWeather::Forecast.city_id(city.owm_city_identifier.to_s, DEFAULT_OWM_PARAMS)
      # return empty array if something went wrong
      return [] unless owm_request && owm_request['list']

      owm_request['list'].map do |forecast|
        # ugly i know. but we need it to pass params to struct correctly
        # also full list is usefull to retrieve data for decorators
        forecast_params = owm_data_to_cached_forecasts(forecast)
        CachedCityForecastBuilder.new(forecast_class).with_params(forecast_params).build
      end
    end

    private

    attr_accessor :owm_default_params

    def owm_data_to_cached_forecasts(forecast)
      {
        datetime:       forecast['dt'] * 1000,
        description_en: forecast['weather'][0]['main'],
        description_ru: forecast['weather'][0]['description'],
        humidity:       forecast['main']['humidity'],
        pressure:       forecast['main']['pressure'],
        temperature:    forecast['main']['temp'],
        weather_icon:   forecast['weather'][0]['icon'],
        wind_deg:       forecast['wind']['deg'],
        wind_speed:     forecast['wind']['speed']
      }
    end
  end
end
