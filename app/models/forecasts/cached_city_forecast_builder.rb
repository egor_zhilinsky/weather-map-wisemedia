module Forecasts
  class CachedCityForecastBuilder
    def initialize(forecast_class)
      self.forecast = forecast_class.new
    end

    def with_params(forecast_params)
      FULL_FORECAST_PARAMS_LIST.each do |param_key|
        forecast[param_key] = forecast_params[param_key]
      end
      self
    end

    def build
      forecast
    end

    private

    attr_accessor :forecast
  end
end
