module Forecasts
  FULL_FORECAST_PARAMS_LIST = %I[
    datetime
    description_en
    description_ru
    humidity
    temperature
    pressure
    weather_icon
    wind_speed
    wind_deg
  ].freeze
end
