require 'rails_helper'

describe Forecasts::CachedCityForecast do
  context '#initialize' do
    it 'init all fields' do
      forecast = FactoryGirl.build(:cached_city_forecast)
      expect(forecast.class).to eq(Forecasts::CachedCityForecast)
      expect(forecast.as_json.symbolize_keys.keys).to eq(Forecasts::FULL_FORECAST_PARAMS_LIST)
    end
  end
end
