require 'rails_helper'

describe Forecasts::OwmAdapter do
  ForecastMockClass = Struct.new(*Forecasts::FULL_FORECAST_PARAMS_LIST)

  context 'fetch openweathermap' do
    context '#forecasts_for' do
      context 'returns parsed data' do
        before do
          owm_data = {
            list: [
              FactoryGirl.build(:owm_forecast),
              FactoryGirl.build(:owm_forecast)
            ]
          }.to_json

          stub_request(:get, /api.openweathermap.org/)
            .to_return(status: 200, body: owm_data)
        end

        it 'returns 2 forecasts' do
          fetched_forecasts = Forecasts::OwmAdapter.new.forecasts_for(City.new, ForecastMockClass)
          expect(fetched_forecasts.size).to eq(2)
        end

        it 'returns converted data' do
          fetched_forecasts = Forecasts::OwmAdapter.new.forecasts_for(City.new, ForecastMockClass)
          expect(fetched_forecasts[0].datetime).to eq(100_000)
          expect(fetched_forecasts[0].wind_speed).to eq(2)
        end

        it 'returns forecast instance' do
          fetched_forecasts = Forecasts::OwmAdapter.new.forecasts_for(City.new, ForecastMockClass)
          expect(fetched_forecasts[0].class).to eq(ForecastMockClass)
        end
      end

      context 'returns empty error' do
        it 'with 500 error' do
          stub_request(:get, /api.openweathermap.org/).to_return(status: 500)
          fetched_forecasts = Forecasts::OwmAdapter.new.forecasts_for(City.new, ForecastMockClass)
          expect(fetched_forecasts).to be_empty
        end

        it 'with 401 error' do
          stub_request(:get, /api.openweathermap.org/).to_return(status: 401)
          fetched_forecasts = Forecasts::OwmAdapter.new.forecasts_for(City.new, ForecastMockClass)
          expect(fetched_forecasts).to be_empty
        end

        it 'with 200 but without requried `list` field' do
          stub_request(:get, /api.openweathermap.org/)
            .to_return(status: 200, body: '{}')
          fetched_forecasts = Forecasts::OwmAdapter.new.forecasts_for(City.new, ForecastMockClass)
          expect(fetched_forecasts).to be_empty
        end
      end
    end
  end
end
