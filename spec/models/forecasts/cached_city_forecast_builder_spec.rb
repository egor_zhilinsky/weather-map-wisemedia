require 'rails_helper'

describe Forecasts::CachedCityForecastBuilder do
  context 'CachedCityForecast' do
    it 'returns CachedCityForecast instance' do
      expect(build_instance.class).to eq(Forecasts::CachedCityForecast)
    end

    describe 'equals to sent params' do
      it 'en' do
        json = build_instance.as_json.symbolize_keys
        expect(json.keys).to eq(Forecasts::FULL_FORECAST_PARAMS_LIST)
      end
    end

    private

    def build_instance
      Forecasts::CachedCityForecastBuilder
        .new(Forecasts::CachedCityForecast)
        .with_params(FactoryGirl.build(:owm_forecast))
        .build
    end
  end
end
