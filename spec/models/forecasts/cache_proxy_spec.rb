require 'rails_helper'

describe Forecasts::CacheProxy do
  context '#all_for' do
    before do
      allow(RedisServer.instance).to receive(:get).and_return(
        [
          FactoryGirl.build(:cached_city_forecast),
          FactoryGirl.build(:cached_city_forecast)
        ].to_json
      )
    end

    let(:city) { FactoryGirl.build(:city) }

    it 'returns 2 forecasts for a city' do
      expect(Forecasts::CacheProxy.new.all_for(city.owm_city_identifier).size).to eq(2)
    end

    it 'returns convert it to forecast instance' do
      expect(Forecasts::CacheProxy.new.all_for(city.owm_city_identifier)[0].class).to(
        eq(Forecasts::CachedCityForecast)
      )
    end

    it 'returns empty array if cache is empty' do
      allow(RedisServer.instance).to receive(:get).and_return(nil)
      expect(Forecasts::CacheProxy.new.all_for(city.owm_city_identifier)).to eq([])
    end
  end

  context '#update_forecasts_for' do
    let(:forecast_data) do
      [
        FactoryGirl.build(:cached_city_forecast),
        FactoryGirl.build(:cached_city_forecast)
      ]
    end

    let(:city) { FactoryGirl.build(:city) }

    it 'sets new forecasts for city' do
      proxy = Forecasts::CacheProxy.new
      proxy.update_forecasts_for(city.owm_city_identifier, forecast_data)
      expect(proxy.all_for(city.owm_city_identifier).size).to eq(2)
    end

    it 'sets empty array for city' do
      proxy = Forecasts::CacheProxy.new
      proxy.update_forecasts_for(city.owm_city_identifier, [])
      expect(proxy.all_for(city.owm_city_identifier)).to eq([])
    end
  end
end
