# == Schema Information
#
# Table name: cities
#
#  id                  :integer          not null, primary key
#  name_ru             :string(200)      not null
#  name_en             :string(200)      not null
#  description_ru      :text(65535)
#  description_en      :text(65535)
#  avatar              :string(255)
#  active              :boolean          default('0')
#  owm_city_identifier :integer          not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'rails_helper'

describe City do
  context 'validation' do
    let(:city) { FactoryGirl.build(:city) }

    it { should validate_presence_of(:owm_city_identifier) }
    it { should validate_presence_of(:name_ru) }
    it { should validate_length_of(:name_ru) }
    it { should validate_presence_of(:name_en) }
    it { should validate_length_of(:name_en) }
    it { should validate_length_of(:description_ru) }
    it { should validate_length_of(:description_en) }

    it 'valid with good data' do
      expect(city.valid?).to be_truthy
    end

    describe 'en' do
      it 'validates max name length(200)' do
        city.name_en = '*' * 201
        expect(city.valid?).to eq(false)
        expect(city.errors[:name_en]).to include('is too long (maximum is 200 characters)')
      end

      it 'validates max desc length(200)' do
        city.description_en = '*' * 5_001
        expect(city.valid?).to eq(false)
        expect(city.errors[:description_en]).to include('is too long (maximum is 5000 characters)')
      end
    end

    describe 'ru' do
      it 'validates max name length(200)' do
        city.name_ru = '*' * 201
        expect(city.valid?).to eq(false)
        expect(city.errors[:name_ru]).to include('is too long (maximum is 200 characters)')
      end

      it 'validates max desc length(200)' do
        city.description_ru = '*' * 5_001
        expect(city.valid?).to eq(false)
        expect(city.errors[:description_ru]).to include('is too long (maximum is 5000 characters)')
      end
    end
  end

  describe '#active' do
    it 'no active' do
      expect(City.active.count).to eq(0)
    end

    it '1 active' do
      FactoryGirl.create(:city, active: false)
      FactoryGirl.create(:city, active: true)
      expect(City.active.count).to eq(1)
    end
  end

  context '#forecasts' do
    let(:city) { FactoryGirl.build(:city) }
    it 'returns city forecasts' do
      expect(city.forecasts).to eq([])
    end

    describe 'returns correct json' do
      it 'en' do
        allow(RedisServer.instance).to receive(:get).and_return([FactoryGirl.build(:cached_city_forecast)].to_json)
        expect(city.forecasts.count).to eq(1)
      end
    end
  end
end
