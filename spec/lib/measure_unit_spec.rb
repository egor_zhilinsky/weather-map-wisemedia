require 'rails_helper'

describe MeasureUnit do
  describe 'converts speed to mph' do
    it { expect(MeasureUnit.to_miles_per_hour(1)).to eq(2.24) }
    it { expect(MeasureUnit.to_miles_per_hour(21.5)).to eq(48.09) }
    it { expect(MeasureUnit.to_miles_per_hour(50.99)).to eq(114.06) }
  end

  describe 'converts to forenheit' do
    it { expect(MeasureUnit.to_fahrenheit(-10.5)).to eq(13.1) }
    it { expect(MeasureUnit.to_fahrenheit(0)).to eq(32) }
    it { expect(MeasureUnit.to_fahrenheit(20)).to eq(68) }
  end

  describe 'validates unit' do
    describe 'valid' do
      it 'is valid' do
        expect(MeasureUnit.new(measure_unit: 'metric').valid?).to be_truthy
        expect(MeasureUnit.new(measure_unit: 'imperial').valid?).to be_truthy
        expect(MeasureUnit.new.valid?).to be_truthy
      end

      it 'returns correct value' do
        expect(MeasureUnit.new(measure_unit: 'metric').measure_unit).to eq('metric')
        expect(MeasureUnit.new.measure_unit).to eq('metric')
        expect(MeasureUnit.new(measure_unit: 'imperial').measure_unit).to eq('imperial')
      end
    end

    describe 'invalid' do
      it 'returns error' do
        unit = MeasureUnit.new(measure_unit: 'foo')
        expect(unit.invalid?).to be_truthy
        expect(unit.errors[:measure_unit]).to include('is not included in the list')
      end
    end
  end
end
