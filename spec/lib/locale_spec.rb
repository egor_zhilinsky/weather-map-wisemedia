require 'rails_helper'

describe Locale do
  describe 'validate' do
    describe 'is valid' do
      it 'valid if locale - en' do
        expect(Locale.new(lang: 'en').valid?).to be_truthy
      end

      it 'valid if locale - ru' do
        expect(Locale.new(lang: 'ru').valid?).to be_truthy
      end

      it 'valid empty' do
        expect(Locale.new(lang: nil).valid?).to be_truthy
      end
    end

    describe 'is invalid' do
      it 'fr - is invalid' do
        expect(Locale.new(lang: 'fr').invalid?).to be_truthy
      end

      it '`123` is invalid' do
        expect(Locale.new(lang: 123).invalid?).to be_truthy
      end
    end
  end

  describe '#locale' do
    it 'returns correct ru locale' do
      expect(Locale.new(lang: 'ru').lang).to eq('ru')
    end

    it 'returns en if empty param' do
      expect(Locale.new.lang).to eq('en')
    end
  end
end
