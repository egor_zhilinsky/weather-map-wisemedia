require 'rails_helper'

RSpec.describe Api::ForecastsController do
  let(:cities_index_uri) { '/api/forecasts' }

  context 'validate measure_unit' do
    describe 'valid response' do
      it 'for empty measure_unit' do
        expect(get(cities_index_uri)).to eq(200)
      end

      it 'for `imperial`' do
        expect(get(cities_index_uri + '?measure_unit=imperial')).to eq(200)
      end

      it 'for `metric`' do
        expect(get(cities_index_uri + '?measure_unit=metric')).to eq(200)
      end
    end

    describe 'invalid response' do
      it 'returns 400. unit is invalid' do
        expect(get(cities_index_uri + '?measure_unit=123')).to eq(400)
        expect(json_body[:data][:errors][:measure_unit]).to include('is not included in the list')
      end

      it 'unit is invalid - in russian' do
        expect(get(cities_index_uri + '?measure_unit=123&lang=ru')).to eq(400)
        expect(json_body[:data][:errors][:measure_unit]).to include('is not included in the list')
      end
    end
  end
end
