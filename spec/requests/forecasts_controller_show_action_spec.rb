require 'rails_helper'

RSpec.describe Api::ForecastsController do
  let(:city_show_uri) { '/api/forecasts/:id' }

  context '#index' do
    let(:city) { FactoryGirl.create(:city, active: true) }

    it 'returns empty array of forecasts' do
      expect(get(city_url(city.id))).to eq(200)
      expect(json_body[:data][:city][:name]).to eq(city.name_en)
      expect(json_body[:data][:city][:description]).to eq(city.description_en)
      expect(json_body[:data][:city][:id]).to eq(city.id)
      expect(json_body[:data][:city][:forecasts]).to eq([])
    end

    it 'returns 404' do
      expect(get(city_url(-1))).to eq(404)
    end

    it 'rerturns 404 for not active' do
      expect(get(city_url(FactoryGirl.create(:city, active: false).id))).to eq(404)
    end

    describe 'returns 3 forecasts' do
      before do
        allow(RedisServer.instance).to receive(:get).and_return(
          [
            FactoryGirl.build(:cached_city_forecast),
            FactoryGirl.build(:cached_city_forecast),
            FactoryGirl.build(:cached_city_forecast)
          ].to_json
        )
      end

      describe 'data depends on param' do
        describe 'on lang param' do
          it 'returns ru' do
            expect(get(city_url(city.id) + '?lang=ru')).to eq(200)
            common_check
            expect(json_body[:data][:city][:name]).to eq(city.name_ru)
            expect(json_body[:data][:city][:description]).to eq(city.description_ru)
            expect(first_forecast[:weather_description]).to eq('Дождливо')
          end

          it 'returns en' do
            expect(get(city_url(city.id) + '?lang=en')).to eq(200)
            common_check
            expect(json_body[:data][:city][:name]).to eq(city.name_en)
            expect(json_body[:data][:city][:description]).to eq(city.description_en)
            expect(first_forecast[:weather_description]).to eq('Cloudly')
          end
        end

        describe 'on measure param' do
          let(:metric_data) { FactoryGirl.build(:cached_city_forecast) }

          it 'returns metric' do
            expect(get(city_url(city.id) + '?measure_unit=metric')).to eq(200)
            common_check
            expect(first_forecast[:temperature]).to eq(metric_data[:temperature])
            expect(first_forecast[:wind_speed]).to eq(metric_data[:wind_speed])
          end

          it 'returns imperial' do
            expect(get(city_url(city.id) + '?measure_unit=imperial')).to eq(200)
            common_check
            expect(first_forecast[:temperature]).to eq(MeasureUnit.to_fahrenheit(metric_data[:temperature]))
            expect(first_forecast[:wind_speed]).to eq(MeasureUnit.to_miles_per_hour(metric_data[:wind_speed]))
          end
        end

        def common_check
          json = json_body[:data]
          expect(json[:city][:id]).to eq(city.id)
          expect(json[:city][:forecasts].size).to eq(3)
          expect(json[:city][:forecasts][0].keys.sort).to eq(
            %i[datetime weather_icon temperature wind_speed humidity pressure weather_description wind_deg].sort
          )
        end
      end
    end

    def city_url(id)
      city_show_uri.gsub(':id', id.to_s)
    end

    def first_forecast
      json_body[:data][:city][:forecasts][0]
    end
  end
end
