require 'rails_helper'

RSpec.describe Api::ForecastsController do
  let(:cities_index_uri) { '/api/forecasts' }

  context '#index' do
    let!(:city_not_active) { FactoryGirl.create(:city, active: false) }

    it 'returns empty array' do
      expect(get(cities_index_uri)).to eq(200)
      expect(json_body[:data]).to eq(cities: [])
    end

    describe 'returns 2 cities' do
      let!(:city1) { FactoryGirl.create(:city, active: true) }
      let!(:city2) { FactoryGirl.create(:city, active: true) }

      before do
        allow(RedisServer.instance).to receive(:get).and_return(
          [
            FactoryGirl.build(:cached_city_forecast),
            FactoryGirl.build(:cached_city_forecast),
            FactoryGirl.build(:cached_city_forecast)
          ].to_json
        )
      end

      describe 'data depends on param' do
        describe 'on lang param' do
          it 'returns ru' do
            expect(get(cities_index_uri + '?lang=ru')).to eq(200)
            common_check
            expect(json_body[:data][:cities][0][:name]).to eq(city1.name_ru)
            expect(first_forecast[:weather_description]).to eq('Дождливо')
          end

          it 'returns en' do
            expect(get(cities_index_uri + '?lang=en')).to eq(200)
            common_check
            expect(json_body[:data][:cities][0][:name]).to eq(city1.name_en)
            expect(first_forecast[:weather_description]).to eq('Cloudly')
          end
        end

        describe 'on measure param' do
          let(:metric_data) { FactoryGirl.build(:cached_city_forecast) }

          it 'returns metric' do
            expect(get(cities_index_uri + '?measure_unit=metric')).to eq(200)
            common_check
            expect(first_forecast[:temperature]).to eq(metric_data.temperature)
          end

          it 'returns imperial' do
            expect(get(cities_index_uri + '?measure_unit=imperial')).to eq(200)
            common_check
            expect(first_forecast[:temperature]).to eq(MeasureUnit.to_fahrenheit(metric_data.temperature))
          end
        end

        def common_check
          json = json_body[:data]
          expect(json[:cities].size).to eq(2)
          expect(json[:cities][0][:id]).to eq(city1.id)
          expect(json[:cities][0][:forecasts].size).to eq(3)
          expect(json[:cities][0][:forecasts][0].keys.sort).to eq(
            %i[weather_description weather_icon temperature].sort
          )
        end
      end
    end

    def first_forecast
      json_body[:data][:cities][0][:forecasts][0]
    end
  end
end
