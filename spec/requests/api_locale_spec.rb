require 'rails_helper'

RSpec.describe Api::ForecastsController do
  let(:cities_index_uri) { '/api/forecasts' }

  context 'validate locale' do
    describe 'valid response' do
      it 'for empty language' do
        expect(get(cities_index_uri)).to eq(200)
      end

      it 'for `ru`' do
        expect(get(cities_index_uri + '?lang=ru')).to eq(200)
      end

      it 'for `en`' do
        expect(get(cities_index_uri + '?lang=ru')).to eq(200)
      end
    end

    describe 'bad response' do
      it 'lang is invalid' do
        expect(get(cities_index_uri + '?lang=123')).to eq(400)
        expect(json_body[:data][:errors][:lang]).to include('is not included in the list')
      end
    end
  end
end
