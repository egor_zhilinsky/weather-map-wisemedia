require 'rails_helper'

describe UpdateForecastsJob do
  context '#perform' do
    let(:city) { FactoryGirl.create(:city) }

    before do
      owm_data = {
        list: [FactoryGirl.build(:owm_forecast)]
      }.to_json

      stub_request(:get, /api.openweathermap.org/)
        .to_return(status: 200, body: owm_data)
    end

    it 'saves forecasts for city' do
      expect(city.forecasts).to eq([])
      UpdateForecastsJob.perform_now
      expect(city.forecasts).not_to eq([])
    end
  end
end
