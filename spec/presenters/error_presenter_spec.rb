require 'rails_helper'

describe ErrorPresenter do
  it 'returns array of errors' do
    expect(ErrorPresenter.new(data: [1, 2]).as_json).to eq(errors: [1, 2])
  end
end
