require 'rails_helper'

describe Forecasts::CityForecastsPresenter do
  context 'decorates array of forecasts' do
    let(:city) { FactoryGirl.build(:city) }

    let(:json) do
      Forecasts::CityForecastsPresenter.new(
        data: city,
        lang: Locale::LANG::EN,
        measure_unit: MeasureUnit::UNIT::METRIC
      ).as_json
    end

    let(:city_json) { json[:city] }
    let(:first_forecast_json) { json[:city][:forecasts][0] }

    describe 'matches json' do
      before do
        allow(RedisServer.instance).to receive(:get).and_return([FactoryGirl.build(:cached_city_forecast)].to_json)
      end

      describe 'en' do
        it { expect(city_json[:description]).to eq(city.description_en) }
        it { expect(city_json[:forecasts].size).to eq(1) }
        it { expect(city_json[:avatar]).to eq(city.avatar.middle) }
        it { expect(city_json[:name]).to eq(city.name_en) }
        it do
          expect(first_forecast_json.keys.sort).to eq(
            %i[datetime weather_icon temperature wind_speed humidity pressure weather_description wind_deg].sort
          )
        end
      end
    end

    it 'returns empty forecasts if there is no data' do
      expect(json[:city][:forecasts].size).to eq(0)
    end
  end
end
