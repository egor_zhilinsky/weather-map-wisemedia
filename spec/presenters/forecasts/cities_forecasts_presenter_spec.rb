require 'rails_helper'

describe Forecasts::CitiesForecastsPresenter do
  context 'decorates array of forecasts' do
    let(:city) { FactoryGirl.build(:city) }
    let(:json) do
      Forecasts::CitiesForecastsPresenter.new(
        data: [city, city],
        lang: Locale::LANG::EN,
        measure_unit: MeasureUnit::UNIT::METRIC
      ).as_json
    end

    let(:first_city_json) { json[:cities][0] }
    let(:first_forecast_json) { json[:cities][0][:forecasts][0] }

    describe 'matches json' do
      before do
        allow(RedisServer.instance).to receive(:get).and_return([FactoryGirl.build(:cached_city_forecast)].to_json)
      end

      describe 'en' do
        it { expect(first_city_json[:forecasts].size).to eq(1) }
        it { expect(first_city_json[:avatar]).to eq(city.avatar.thumb) }
        it { expect(first_city_json[:name]).to eq(city.name_en) }
        it do
          expect(first_forecast_json.keys.sort).to eq(
            %i[weather_icon temperature weather_description].sort
          )
        end
      end
    end
  end
end
