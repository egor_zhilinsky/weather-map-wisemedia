require 'rails_helper'

describe Forecasts::CityForecastLanguageMeasureDecorator do
  let(:forecast) { forecast_decorator.forecast }

  describe 'en & metric' do
    let(:forecast_decorator) { FactoryGirl.build(:city_forecast_language_measure_decorator) }

    describe 'instance methods' do
      it { expect(forecast_decorator.datetime).to eq(forecast.datetime) }
      it { expect(forecast_decorator.humidity).to eq(forecast.humidity) }
      it { expect(forecast_decorator.pressure).to eq(forecast.pressure) }
      it { expect(forecast_decorator.wind_deg).to eq(forecast.wind_deg) }
      it { expect(forecast_decorator.wearher_description).to eq(forecast.description_en) }
      it { expect(forecast_decorator.weather_icon).to eq(forecast.weather_icon) }
      it { expect(forecast_decorator.temperature).to eq(forecast.temperature) }
      it { expect(forecast_decorator.wind_speed).to eq(forecast.wind_speed) }
    end
  end

  describe 'ru & metric' do
    let(:forecast_decorator) { FactoryGirl.build(:city_forecast_language_measure_decorator, :ru) }

    describe 'instance methods' do
      it { expect(forecast_decorator.datetime).to eq(forecast.datetime) }
      it { expect(forecast_decorator.humidity).to eq(forecast.humidity) }
      it { expect(forecast_decorator.pressure).to eq(forecast.pressure) }
      it { expect(forecast_decorator.wind_deg).to eq(forecast.wind_deg) }
      it { expect(forecast_decorator.wearher_description).to eq(forecast.description_ru) }
      it { expect(forecast_decorator.weather_icon).to eq(forecast.weather_icon) }
      it { expect(forecast_decorator.temperature).to eq(forecast.temperature) }
      it { expect(forecast_decorator.wind_speed).to eq(forecast.wind_speed) }
    end
  end

  describe 'en & imperial' do
    let(:forecast_decorator) { FactoryGirl.build(:city_forecast_language_measure_decorator, :imperial) }

    describe 'instance methods' do
      it { expect(forecast_decorator.datetime).to eq(forecast.datetime) }
      it { expect(forecast_decorator.humidity).to eq(forecast.humidity) }
      it { expect(forecast_decorator.pressure).to eq(forecast.pressure) }
      it { expect(forecast_decorator.wind_deg).to eq(forecast.wind_deg) }
      it { expect(forecast_decorator.wearher_description).to eq(forecast.description_en) }
      it { expect(forecast_decorator.weather_icon).to eq(forecast.weather_icon) }
      it { expect(forecast_decorator.temperature).to eq(::MeasureUnit.to_fahrenheit(forecast.temperature)) }
      it { expect(forecast_decorator.wind_speed).to eq(::MeasureUnit.to_miles_per_hour(forecast.wind_speed)) }
    end
  end
end
