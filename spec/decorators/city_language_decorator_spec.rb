require 'rails_helper'

describe CityLanguageDecorator do
  let(:city) { city_decorator.city }

  describe 'en' do
    let(:city_decorator) { FactoryGirl.build(:city_language_decorator) }

    describe 'instance methods' do
      it { expect(city_decorator.id).to eq(city.id) }
      it { expect(city_decorator.avatar).to eq(city.avatar) }
      it { expect(city_decorator.name).to eq(city.name_en) }
      it { expect(city_decorator.description).to eq(city.description_en) }
      it { expect(city_decorator.language).to eq(Locale::LANG::EN) }
    end
  end

  describe 'ru' do
    let(:city_decorator) { FactoryGirl.build(:city_language_decorator, :ru) }

    describe 'instance methods' do
      it { expect(city_decorator.id).to eq(city.id) }
      it { expect(city_decorator.avatar).to eq(city.avatar) }
      it { expect(city_decorator.name).to eq(city.name_ru) }
      it { expect(city_decorator.description).to eq(city.description_ru) }
      it { expect(city_decorator.language).to eq(Locale::LANG::RU) }
    end
  end
end
