# == Schema Information
#
# Table name: cities
#
#  id                  :integer          not null, primary key
#  name_ru             :string(200)      not null
#  name_en             :string(200)      not null
#  description_ru      :text(65535)
#  description_en      :text(65535)
#  avatar              :string(255)
#  active              :boolean          default("0")
#  owm_city_identifier :integer          not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

FactoryGirl.define do
  factory :city do
    sequence(:name_ru) { |n| "город_#{n}" }
    sequence(:name_en) { |n| "city_name_#{n}" }
    sequence(:description_ru) { |n| "город_описание_#{n}" }
    sequence(:description_en) { |n| "city_desc_#{n}" }
    sequence(:owm_city_identifier) { |n| n }
    sequence(:avatar) { |n| "avatar_#{n}" }
  end

  factory :city_language_decorator do
    initialize_with do
      new(city: FactoryGirl.build(:city), language: Locale::LANG::EN)
    end

    trait :ru do
      initialize_with do
        new(city: FactoryGirl.build(:city), language: Locale::LANG::RU)
      end
    end
  end
end
