FactoryGirl.define do
  factory :cached_city_forecast, class: Forecasts::CachedCityForecast do
    datetime     100
    temperature  256
    weather_icon 'io20'
    pressure     10
    humidity     20
    wind_speed   3
    wind_deg     180
    description_en 'Cloudly'
    description_ru 'Дождливо'
  end

  factory :city_forecast_language_measure_decorator, class: Forecasts::CityForecastLanguageMeasureDecorator do
    initialize_with do
      new(forecast: FactoryGirl.build(:cached_city_forecast),
        language: Locale::LANG::EN, measure_unit: MeasureUnit::UNIT::METRIC)
    end

    trait :ru do
      initialize_with do
        new(forecast: FactoryGirl.build(:cached_city_forecast),
          language: Locale::LANG::RU, measure_unit: MeasureUnit::UNIT::METRIC)
      end
    end

    trait :imperial do
      initialize_with do
        new(forecast: FactoryGirl.build(:cached_city_forecast),
          language: Locale::LANG::EN, measure_unit: MeasureUnit::UNIT::IMPERIAL)
      end
    end
  end
end
