FactoryGirl.define do
  factory :owm_forecast, class: Hash do
    initialize_with do
      {
        dt: 100,
        main: {
          temp: -3,
          pressure: 10,
          humidity: 20
        },
        weather: [
          description: 'sunny',
          icon: '10ico'
        ],
        wind: {
          speed: 2,
          deg: 270
        }
      }
    end
  end
end
